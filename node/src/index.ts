'use strict';

import WebSocket = require("ws");
import * as winston from "winston";
import {Application} from "./application";

const application = new Application(9652);
application.start();

// Configure logger
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {timestamp: true});

// Exit strategies
process.on('exit', (code) => {
    console.info('Received exit code', code);
    application.stop();
});

process.on('SIGINT', () => {
    process.exit(128 + 2);
});

process.on('SIGTERM', () => {
    process.exit(128 + 15);
});