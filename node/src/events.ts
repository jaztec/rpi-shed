export interface PinEvent {
    pin: number;
    state: boolean;
    ready: boolean;
}