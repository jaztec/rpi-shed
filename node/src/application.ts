'use strict';

import WebSocket = require("ws");
import * as winston from "winston";
import {PIN_ALL_OFF, PIN_ALL_ON, PIN_OFF, PIN_ON, Rpi, STATE} from "./rpi";
import {PinEvent} from "./events";

export class Application {
    private _server: WebSocket.Server;
    private _logger: winston.Winston = winston;
    private _rpi: Rpi = new Rpi();

    constructor(private _port: number) { }

    start(): void {
        this._server = new WebSocket.Server({port: this._port});
        this.bindSocketEvents();
        this._logger.info('Application running on port', this._port);
    }

    stop(): void {
        this._logger.info('Application running on port', this._port, 'stopped');
    }

    private bindSocketEvents(): void {

        this._rpi.on('change', (event: PinEvent) => {
            this._server.clients.forEach((socket) => {
                try {
                    socket.send(JSON.stringify(event));
                } catch (e) {
                    this._logger.error(e);
                }
            });
        });

        this._server.on('connection', (socket) => {
            this._logger.info('Incoming connection');

            socket.on('message', (message) => {
                let data: any;

                try {
                    data = JSON.parse(message.toString());
                } catch (e) {
                    this._logger.error(e);
                    return;
                }

                if (data.state === STATE) {
                    this._rpi.triggerPinUpdates();
                }

                if ((data.state !== PIN_ALL_ON || data.state !== PIN_ALL_OFF) && (data.pin < 0 || data.pin > 7)) return;

                switch (data.state) {
                    case PIN_ON: this._rpi.pinOn(data.pin); break;
                    case PIN_OFF: this._rpi.pinOff(data.pin); break;
                    case PIN_ALL_ON: this._rpi.allOn(); break;
                    case PIN_ALL_OFF: this._rpi.allOff(); break;
                }
            });
        });
    }

}