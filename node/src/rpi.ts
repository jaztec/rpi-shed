import {EventEmitter} from "events";
import {PinEvent} from "./events";
import * as winston from "winston";
import * as gpio from "rpi-gpio";

export const PINS = [11, 13, 15, 29, 31, 33, 35, 37];
export const PIN_ON = 'on';
export const PIN_OFF = 'off';
export const PIN_ALL_ON = 'all-on';
export const PIN_ALL_OFF = 'all-off';
export const STATE = 'state';

interface PinCollection<Pin> {
    [key: string]: Pin;
}

export class Pin extends EventEmitter {
    private _logger: winston.Winston = winston;
    private _ready = false;
    private _state = false;

    constructor(private _channel: number, private _index: number, mode: string) {
        super();
        gpio.setup(this.channel, mode, (err) => {
            if (err) this._logger.error(err);
            else {
                this._ready = true;
                this.emit('ready', this.event);
            }
        });
    }

    get channel(): number {
        return this._channel;
    }

    get ready(): boolean {
        return this._ready;
    }

    get state(): boolean {
        return this._state;
    }

    get index(): number {
        return this._index;
    }

    get event(): PinEvent {
        return {
            pin: this.index,
            state: this.state,
            ready: this.ready
        };
    }

    turnOn(): void {
        this.toggle(true);
    }

    turnOff(): void {
        this.toggle(false);
    }

    private toggle(value: boolean): void {
        if (this.ready) gpio.write(this.channel, value, err => {
            this._state = value;
            if (err) return;
            this.emit('change', this.event);
        });
    }
}

export class Rpi extends EventEmitter {
    private _pins: PinCollection<Pin> = {};

    constructor() {
        super();
        PINS.forEach((channel, index) => {
            let pin = new Pin(channel, index, gpio.DIR_LOW);
            pin.on('ready', event => this.emit('change', event));
            pin.on('change', event => this.emit('change', event));
            this._pins[index] = pin;
        });
    }

    triggerPinUpdates(): void {
        for (let pin in this._pins) this.emit('change', this._pins[pin].event);
    }

    pinOn(index: number): void {
        if (this._pins[index]) this._pins[index].turnOn();
    }

    pinOff(index: number): void {
        if (this._pins[index]) this._pins[index].turnOff();
    }

    allOn(): void {
        for (let pin in this._pins) this._pins[pin].turnOn();
    }

    allOff(): void {
        for (let pin in this._pins) this._pins[pin].turnOff();
    }
}