import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SwitchComponent} from "./switch/switch.component";
import {RpiService} from "./rpi/rpi.service";
import {SwitchListComponent} from "./switch-list/switch-list.component";

@NgModule({
  declarations: [
    AppComponent,
    SwitchComponent,
    SwitchListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    RpiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
