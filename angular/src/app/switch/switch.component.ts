import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Toggle, ToggleEvent} from "../rpi/toggle";

@Component({
    selector: 'switch-component',
    templateUrl: 'switch.component.html',
    styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit{
    @Input() toggle: Toggle;
    @Output('state') stateEmitter: EventEmitter<ToggleEvent> = new EventEmitter<ToggleEvent>();
    public name: string;

    constructor () {
        this.click(false);
    }

    get switchState(): boolean {
        return this.toggle ? this.toggle.state : false;
    }

    ngOnInit(): void {
        if (this.toggle)
            this.name = 'Relais ' + this.toggle.port.toString();
    }

    click(state: boolean) {
        if (!this.toggle) return;
        this.toggle.state = state;
        this.stateEmitter.emit({
            toggle: this.toggle,
            state: state
        });
    }
}