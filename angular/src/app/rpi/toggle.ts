export interface ToggleCommand {
    pin?: number,
    state: string
}

export interface ToggleEvent {
    toggle: Toggle,
    state: boolean
}

export class Toggle {
    public state: boolean;
    public name: string;

    constructor (private _mappedPort: number) { }

    get port(): number {
        return this._mappedPort;
    }
}