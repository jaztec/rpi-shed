import {EventEmitter, Injectable} from "@angular/core";
import {Toggle, ToggleCommand} from "./toggle";
import {PinEvent} from "../../../../node/src/events";

@Injectable()
export class RpiService extends EventEmitter<PinEvent> {
    private _socket: WebSocket;

    constructor() {
        super();
        this._socket = new WebSocket('ws://raspberrypi:9652');
        this._socket.onopen = (_) => {
            this.triggerStatusUpdates();
        };
        this._socket.onmessage = event => {
            let data: PinEvent;
            try {
                data = JSON.parse(event.data);
            } catch (e) {
                console.error(e);
                return;
            }
            this.emit(data);
        };
    }

    turnPin(toggle: Toggle, state: boolean) {
        this.emitCommand({
            pin: toggle.port,
            state: state ? 'on' : 'off'
        });
    }

    triggerStatusUpdates() {
        this.emitCommand({
            state: 'state'
        });
    }

    private emitCommand(command: ToggleCommand) {
        if (this._socket.readyState === WebSocket.OPEN)
            this._socket.send(JSON.stringify(command));
    }
}