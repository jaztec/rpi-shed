import {Component, OnInit} from "@angular/core";
import {Toggle, ToggleEvent} from "../rpi/toggle";
import {RpiService} from "../rpi/rpi.service";

@Component({
    selector: 'switch-list',
    templateUrl: './switch-list.component.html',
    styleUrls: ['./switch-list.component.scss']
})
export class SwitchListComponent implements OnInit {
    toggles: Array<Toggle> = [];

    constructor (private _rpiService: RpiService) {
        for (let i: number = 0; i < 8; i++)
            this.toggles.push(new Toggle(i));
        this._rpiService.subscribe(event => {
            if (this.toggles[event.pin])
                this.toggles[event.pin].state = event.state;
        });
    }

    ngOnInit(): void {
        this._rpiService.triggerStatusUpdates();
    }

    processState(event: ToggleEvent) {
        this._rpiService.turnPin(event.toggle, event.state);
    }
}